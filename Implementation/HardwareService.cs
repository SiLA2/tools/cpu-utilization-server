﻿using Hsrm.Sila2.CpuUtilizationService.Contracts;
using LibreHardwareMonitor.Hardware;
using LibreHardwareMonitor.Hardware.Cpu;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Hsrm.Sila2.CpuUtilizationService.Implementation
{
    [Export(typeof(ICpuUtilizationService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    internal class HardwareService : ICpuUtilizationService
    {
        private readonly Computer _computer;
        private readonly GenericCpu _cpu;

        public HardwareService()
        {
            _computer = new Computer { IsCpuEnabled = true };
            _computer.Open();

            _cpu = _computer.Hardware.OfType<GenericCpu>().FirstOrDefault(h => h.HardwareType == HardwareType.Cpu);
        }

        public string CpuModel => _cpu.Name;

        public IEnumerable<double> CpuUtilization
        {
            get
            {
                _cpu.Update();
                return
                   (from sensor in _cpu.Sensors
                    where sensor.SensorType == SensorType.Load
                    select (double)sensor.Value.GetValueOrDefault(float.NaN)).Take(_cpu.CpuId.Length);
            }
        }
    }
}
