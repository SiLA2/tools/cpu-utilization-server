﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2;

namespace Hsrm.Sila2.CpuUtilizationService.Contracts
{
    /// <summary>
    /// Denotes a service to obtain CPU utilization details
    /// </summary>
    [SilaFeature]
    public interface ICpuUtilizationService
    {
        /// <summary>
        /// Gets the CPU model and make
        /// </summary>
        string CpuModel { get; }

        /// <summary>
        /// Gets the current utilization for all CPU cores
        /// </summary>
        IEnumerable<double> CpuUtilization { get; }
    }
}
