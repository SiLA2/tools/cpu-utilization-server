$path = Get-ChocolateyPath -PathType 'PackagePath'
$desktop = [Environment]::GetFolderPath("Desktop")
Install-ChocolateyShortcut -ShortcutFilePath "$desktop\CPU Utilization Server.lnk" -TargetPath "$path\tools\CpuUtilizationServer.exe"