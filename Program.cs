﻿using Tecan.Sila2.Server;

namespace Hsrm.Sila2.CpuUtilizationService
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Bootstrapper.Start(args).RunUntilSigInt();
        }
    }
}
